# aime-card-manager

A basic tool for managing emulated Aime cards.

# How to use

1. Configure `aime-card-manager.ini`:

- Set `path` to the location of your `felica.txt` file
- (Optional) set `msgtimeout` - this is the number of milliseconds tooltip messages will remain on screen
- (Optional) populate `[Cards]` with one or more entries in the format `<PROFILE>=<FELICA_ID>` - the profile value is cosmetic only, and has no relation to the profile name saved by the Aime server

2. Run `aime-card-manager.exe`

- This is an AutoIt3 script compiled using Aut2Exe
- You can check the source/build it yourself from `index.au3`

3. Use the hotkeys to manage your cards during gameplay

# Hotkeys

### Show info: `Ctrl+Alt+I`

Shows the currently active card

### Switch active card: `Ctrl+Alt+S`

Activates a different card. Press the `Ctrl+<NUMBER>` key combo to switch to the corresponding card. Switching card during a credit might do weird things - I haven't tested it.

> (Your `felica.txt` file will be backed-up as `felica.bak`, but you should make your own backup to be safe if you care about your card ID)

### New card: `Ctrl+Alt+N`

Blanks out your card ID. Card-in on your game to generate a new FeliCa ID (press `Ctrl+Alt+I` after carding in to check). Once a new card ID has been generated, press `Ctrl+S` to save it to `aime-card-manager.ini`.

### Quit: `Ctrl+Alt+Q`

Exits the program
