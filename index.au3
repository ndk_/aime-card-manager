#include <Array.au3>
#include <AutoItConstants.au3>
#include <FileConstants.au3>

Opt("MustDeclareVars", 1)

; Hotkey setup
HotKeySet("^!i", "ShowInfo") ; Ctrl+Alt+I = Show info
HotKeySet("^!s", "SwitchCard") ; Ctrl+Alt+S = Switch card
HotKeySet("^!n", "NewCard") ; Ctrl+Alt+N = New/blank card
HotKeySet("^!q", "Quit") ; Ctrl+Alt+Q = Quit

; Function declarations
Func Quit()
  Exit
EndFunc

Func SwitchCard()
  Local $sSelectMsg = "Pick card:" & @CRLF & "0: Cancel" & @CRLF
  Local $iMaxCards = UBound($aAvailableCards, $UBOUND_ROWS)

  For $i = 0 To 9
    If $i < $iMaxCards Then
      If $i > 0 Then
        $sSelectMsg = $sSelectMsg & "Ctrl+" & $i & ": " & $aAvailableCards[$i][0] & " (" & $aAvailableCards[$i][1] & ")" & @CRLF
      EndIf

      HotKeySet("^" & $i, "NumberKeyHandler")
    EndIf
  Next

  Msg($sSelectMsg)
EndFunc

Func NumberKeyHandler()
  Local $iSelection = Int(StringReplace(@HotKeyPressed, "^", "")) ; Extract number part of hotkey combo
  
  If $iSelection > 0 And $iSelection < UBound($aAvailableCards, $UBOUND_ROWS) Then
    Msg("Activate card: " & $aAvailableCards[$iSelection][1])
    WriteFelicaId($aAvailableCards[$iSelection][1])
  Else
    ShowInfo()
  EndIf
  
  For $i = 0 To 9
    HotKeySet("^" & $i)
  Next
EndFunc

Func NewCard()
  WriteFelicaId("")
  ShowInfo()
EndFunc

Func ShowInfo()
  UpdateActiveCard()

  If $sActiveProfileName <> "" Then
    Msg("Active card: " & $sActiveProfileName & " (" & $sActiveFelicaId & ")" & @CRLF & "Found " & $aAvailableCards[0][0] & " available cards")
  ElseIf $sActiveFelicaId <> "" Then
    Msg("New card active: " & $sActiveFelicaId & " press Ctrl+S to save" & @CRLF & "Found " & $aAvailableCards[0][0] & " available cards")
    HotKeySet("^s", "SaveNewCard")
  Else
    Msg("Blank card active (FeliCa ID will be generated at next card-in)" & @CRLF & "Found " & $aAvailableCards[0][0] & " available cards")
  EndIf
EndFunc

Func Msg($sMessage)
  Tooltip($sMessage, 0, 0, "aime-card-manager", 1)
  $hLastMsg = TimerInit()
EndFunc

Func GetCurrentFelicaId()
  Local $hFelica = FileOpen($sFelicaPath, $FO_UTF8_NOBOM)
  Local $sId = FileReadLine($hFelica)
  FileClose($hFelica)

  If @error > 0 Then
    Return ""
  EndIf

  Return $sId
EndFunc

Func WriteFelicaId($sNewId)
   Local $hFelica = FileOpen($sFelicaPath, $FO_OVERWRITE)
   FileWrite($hFelica, $sNewId & @CRLF)
   FileClose($hFelica)

   UpdateActiveCard()
EndFunc

Func FindProfileName()
  Local $iProfileCount = UBound($aAvailableCards, $UBOUND_ROWS) - 1
  Local $sMatchedProfileName = ""

  If IsArray($aAvailableCards) Then
    For $i = 1 To $iProfileCount
      If ($aAvailableCards[$i][1] == $sActiveFelicaId) Then
        $sMatchedProfileName = $aAvailableCards[$i][0]
      EndIf
    Next
  EndIf

  Return $sMatchedProfileName
EndFunc

Func UpdateActiveCard()
  $sActiveFelicaId = GetCurrentFelicaId()
  $sActiveProfileName = FindProfileName()
EndFunc

Func SaveNewCard()
  HotKeySet("^s")
  If $sActiveProfileName == "" And $sActiveFelicaId <> "" Then
    Local $sNewProfileName = "Profile" & UBound($aAvailableCards, $UBOUND_ROWS)
    Local $aNewCard[1][2] = [[$sNewProfileName, $sActiveFelicaId]]
    Local $iNewLength = _ArrayAdd($aAvailableCards, $aNewCard)

    $aAvailableCards[0][0] = $iNewLength

    UpdateIni()
    UpdateActiveCard()

    Msg("Saved " & $sNewProfileName)
  EndIf
EndFunc

Func UpdateIni()
  IniWrite($sConfigFile, "General", "path", $sFelicaPath)
  IniWrite($sConfigFile, "General", "msgtimeout", $iMsgTimeout)

  If IsArray($aAvailableCards) Then
    IniWriteSection($sConfigFile, "Cards", $aAvailableCards)
  Else
    IniWriteSection($sConfigFile, "Cards", "Profile1=" & $sActiveFelicaId)
  EndIf

  $aAvailableCards = GetCardList()
EndFunc

Func GetCardList()
  Local $aCards = IniReadSection($sConfigFile, "Cards") ; 1-indexed array, 0th element is the count
  Local $aEmptyCardList[2][2] = [[1, ""], ["Profile1", GetCurrentFelicaId()]]

  If @error > 0 Then
    Return $aEmptyCardList
  Else
    Return $aCards
  EndIf
EndFunc

; Initial state
Const $sConfigFile = @ScriptDir & "\aime-card-manager.ini"
Global $sFelicaPath = IniRead($sConfigFile, "General", "path", "C:\Chunithm\app\bin\DEVICE\felica.txt")
Global $sBackupPath = StringRegExpReplace($sFelicaPath, "\.txt$", ".bak")
Global $iMsgTimeout = IniRead($sConfigFile, "General", "msgtimeout", 5000)
Global $aAvailableCards = GetCardList()

Global $hLastMsg = TimerInit()
Global $sActiveFelicaId = GetCurrentFelicaId()
Global $sActiveProfileName = FindProfileName()

Global $sState = "idle"

If Not FileExists($sConfigFile) Then
  ; Initialise INI
  UpdateIni()
EndIf

If Not FileExists($sFelicaPath) Then
  MsgBox(16, "aime-card-manager", "felica.txt not found at " & $sFelicaPath & @CRLF & "Check path setting in aime-card-manager.ini")
  Exit
EndIf

If FileExists($sFelicaPath) And Not FileExists($sBackupPath) Then
  ; Backup original felica.txt
  FileCopy($sFelicaPath, $sBackupPath)
EndIf

ShowInfo()

; Main loop
While(1)
  Sleep(100)
  
  If TimerDiff($hLastMsg) > 5000 Then
    Msg("")
  EndIf
WEnd